import os

from resurce import SCAN_PATH, FILE_PATH

with open(FILE_PATH + ".txt", "w") as a:
    for path, subdirs, files in os.walk(SCAN_PATH):
        for filename in files:
            a.write(str(filename) + os.linesep)

