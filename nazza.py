#!/usr/bin/python

import os

try:
    from Tkinter import Frame, Button, TOP, Tk
except ImportError:
    from tkinter import Frame, Button, TOP, Tk

from resurce import PING_CARTELLA_SCANSIONI, PING_WATSON, PING_HOLMES


class App:
    def __init__(self, master):
        frame = Frame(master)
        frame.pack()
        Button(frame, text="Holmes", command=self.holmessh).pack(side=TOP)
        Button(frame, text="Watson", command=self.watsonsh).pack(side=TOP)
        Button(frame, text="Scansioni", command=self.scansh).pack(side=TOP)
        Button(frame, text="Quit", command=frame.quit).pack(side=TOP)

    def scansh(self):
        os.system(PING_CARTELLA_SCANSIONI)

    def watsonsh(self):
        os.system(PING_WATSON)

    def holmessh(self):
        os.system(PING_HOLMES)


root = Tk()
app = App(root)
root.mainloop()
